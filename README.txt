## Summary
Allowing to establish a base shipping rate that is incremented by the weight of
the product. For example, $5 as a base rate and an additional $1 for each extra
lb/kg..., in combination with Address Zone module, you can create complex
matrices as:
             <= 3kg | Every extra kg |   Zone
 Normal    |   3€   |       1€       |  Europe
 Normal    |   6€   |       3€       |   Asia
 Express   |   9€   |       2€       |  Europe
 Express   |  15€   |       7€       |   Asia

## Requirements
 1. Drupal modules:
    - Commerce Physical Product  => https://drupal.org/project/commerce_physical
    - Commerce Shipping  => https://drupal.org/project/commerce_shipping
    - Commerce Flat Rate => https://drupal.org/project/commerce_shipping_flat_rate

## Recommendations
 1. Drupal modules:
      - Address Zone and Commerce Address Zone submodule.
          => https://drupal.org/project/address_zone

## Installation
 1. Place this module, the requirements and/or the recommendations in the folder
    sites/[all]/modules/contrib.
 2. Go to the Module page at Administer > Modules and enable all modules.

## Configuration and usage instructions
 1. Add a new flat rate service and configure its component adding the required
    conditions.
 2. Add a new shipping rate calculation rule with:
    Conditions:
      - 'Compare the shipping line item service' -> the new flat rate service.
      - (Optional - Requires Commerce Address Zone) 'Order address belongs to
        zone' -> the flat rate zone.
    Actions:
      - 'Calculate and apply an order shipping rate based on its weight' with
        your shipping tariff values.
 3. Repeat if it is necessary with every shipping rate and/or zone.

## Sponsors
 * This Drupal 7 module is maintained and developed by http://cambrico.net/
   Get in touch with us for customizations and consultancy:
   http://cambrico.net/contact
 * Sponsored by http://lanasyovillos.com

## About the authors
 * Pedro Cambra (https://www.drupal.org/u/pcambra)
 * Manuel Egío (https://www.drupal.org/u/facine)
