<?php

/**
 * @file
 * Rules integration for the Commerce Shipping Incremental Weight module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_shipping_incremental_weight_rules_condition_info() {
  return array(
    'commerce_shipping_incremental_weight_shipping_service' => array(
      'label' => t('Compare the shipping line item service'),
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line item'),
        ),
        'shipping_service' => array(
          'type' => 'list<text>',
          'label' => t('Shipping service'),
          'description' => t('The shipping service type(s) to look for in the line item.'),
          'options list' => 'commerce_shipping_incremental_weight_shipping_service_options',
          'restriction' => 'input',
        ),
      ),
      'group' => t('Commerce Shipping'),
    ),
  );
}

/**
 * Options list callback: Shipping services for comparison condition.
 */
function commerce_shipping_incremental_weight_shipping_service_options() {
  $shipping_services = commerce_shipping_services();

  $options = array();
  foreach ($shipping_services as $service_id => $service) {
    $options[$service_id] = $service['display_title'];
  }

  return $options;
}

/**
 * Condition callback: Checks to see if a particular shipping service type exist
 * on a line item.
 *
 * @param object $commerce_line_item
 *   The commerce line item object.
 * @param array $shipping_services
 *   The shipping services keyed by its name to search in the line item.
 *
 * @return boolean
 *   TRUE if the line item is the specified shipping service type, otherwise
 *   FALSE.
 */
function commerce_shipping_incremental_weight_shipping_service($commerce_line_item, $shipping_services) {
  if (isset($commerce_line_item->data['shipping_service']['name'])) {
    return array_key_exists($commerce_line_item->data['shipping_service']['name'], $shipping_services);
  }

  return FALSE;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_shipping_incremental_weight_rules_action_info() {
  return array(
    'commerce_shipping_incremental_weight_calculate_apply_rate' => array(
      'label' => t('Calculate and apply an order shipping rate based on its weight'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
        ),
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Shipping line item'),
        ),
        'base_rate' => array(
          'type' => 'commerce_price',
          'label' => t('Base rate'),
        ),
        'base_rate_weight' => array(
          'type' => 'decimal',
          'label' => t('Weight for the previous parameter.'),
        ),
        'extra_unit_rate' => array(
          'type' => 'commerce_price',
          'label' => t('Rate for every extra unit'),
        ),
        'weight_unit' => array(
          'type' => 'text',
          'label' => t('Weight unit'),
          'options list' => 'physical_weight_unit_options',
          'restriction' => 'input',
          'default value' => 'kg',
        ),
      ),
      'group' => t('Commerce Shipping'),
    ),
  );
}

/**
 * Rules action: Calculate an order shipping rate based on its weight.
 */
function commerce_shipping_incremental_weight_calculate_apply_rate($order, $line_item, $base_rate, $base_rate_weight, $extra_unit_rate, $unit) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $order_weight = commerce_physical_order_weight($order, $unit);

  // Build a price array for shipping rate price with the minimum possible.
  $shipping_rate_price = array(
    'amount' => $base_rate['amount'],
    'currency_code' => $line_item_wrapper->commerce_unit_price->currency_code->value(),
    'data' => array(),
  );

  // Check if the order weight is greater than the minimum weight rate.
  if ($order_weight['weight'] > $base_rate_weight) {
    $shipping_rate_price['amount'] += ceil($order_weight['weight'] - $base_rate_weight) * $extra_unit_rate['amount'];
  }

  // Set the price component type and removed previous if exists.
  $price_component = $line_item->data['shipping_service']['price_component'];
  $unit_price = $line_item_wrapper->commerce_unit_price->value();
  $unit_price['data'] = commerce_price_component_delete($unit_price, $price_component);
  $line_item_wrapper->commerce_unit_price->set($unit_price);

  // Add the calculated price to the line item unit price.
  $line_item_wrapper->commerce_unit_price->amount = $shipping_rate_price['amount'];

  // Add the calculated shipping fee component to the unit price.
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
    $line_item_wrapper->commerce_unit_price->value(),
    $price_component,
    $shipping_rate_price,
    TRUE,
    FALSE
  );
}
